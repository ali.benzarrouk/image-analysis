#!/usr/bin/env bash

echo "STARTING PHOTO ANALYSIS"

APPLICATION_ENV=`env | grep ^application\\.env= | cut -d= -f2-`

export JAVA_OPTS="-Dspring.profiles.active=$APPLICATION_ENV \
                    -Dsun.net.inetaddr.ttl=10"

cat ./webapps/ROOT/WEB-INF/classes/application.properties

echo "JAVA_OPTS: $JAVA_OPTS"

sh /usr/local/tomcat/bin/catalina.sh run