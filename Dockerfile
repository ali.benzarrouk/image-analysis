FROM tomcat:9.0.17-jre11-slim
MAINTAINER Ali Ben Zarrouk <ali.benzarrouk@gmail.com>

ARG VERSION
ENV VERSION=${VERSION:-DEVELOP-SNAPSHOT}

LABEL photo-analysis.photo-analysis.version=$VERSION

#MAVEN
ENV MAVEN_VERSION_MAJOR 3
ENV MAVEN_VERSION_MINOR 6.0

RUN wget http://apache.mirrors.pair.com/maven/maven-${MAVEN_VERSION_MAJOR}/${MAVEN_VERSION_MAJOR}.${MAVEN_VERSION_MINOR}/binaries/apache-maven-${MAVEN_VERSION_MAJOR}.${MAVEN_VERSION_MINOR}-bin.tar.gz
RUN tar xvf apache-maven-${MAVEN_VERSION_MAJOR}.${MAVEN_VERSION_MINOR}-bin.tar.gz
RUN rm apache-maven-${MAVEN_VERSION_MAJOR}.${MAVEN_VERSION_MINOR}-bin.tar.gz
RUN mv apache-maven-${MAVEN_VERSION_MAJOR}.${MAVEN_VERSION_MINOR}  /usr/local/apache-maven
ENV M2_HOME=/usr/local/apache-maven
ENV M2=$M2_HOME/bin
ENV PATH=$M2:$PATH

RUN mkdir /usr/local/Photo-Analysis/ && \
    mkdir /usr/local/Photo-Analysis/Back-Up

RUN rm -rf /usr/local/tomcat/webapps/*

ADD ./* /usr/local/Photo-Analysis/
RUN cd /usr/local/Photo-Analysis/ && mvn clean package && mv ./target/photo-analysis-$VERSION.war /usr/local/tomcat/webapps

ADD ./entrypoint.sh entrypoint.sh

RUN mv /usr/local/tomcat/webapps/photo-analysis-$VERSION.war /usr/local/tomcat/webapps/ROOT.war && \
    mkdir -p /usr/local/tomcat/webapps/ROOT && \
    unzip /usr/local/tomcat/webapps/ROOT.war -d /usr/local/tomcat/webapps/ROOT/ && \
    rm -rf /usr/local/tomcat/webapps/ROOT.war

RUN chmod 111 ./entrypoint.sh

ENV PORT 8080
EXPOSE 8080

CMD ["/usr/local/tomcat/entrypoint.sh"]
