# Setting up localstack environment for ChannelActivationService development and integration testing

This directory contains the provisioning scripts for the AWS localstack environment. It also contains an Dockerfile in the awscli folder
which is used by docker-compose to provision the localstack environment.

## Start localstack container for dev
This is a script that will spin up all the containers needed for dev.
From the command line go into the directory containing the start-localDev.sh and run the script.

## Start localstack container for test
This is a script that will spin up all the containers needed for dev and the dockerized and containerized application.
From the command line go into the directory containing the start-localTest.sh and run the script.

Docker compose will bring up the Kafka and zookeeper on the localhost. 

## Provisioning

Provisioning is run as part of docker-compose up for dev and test.

To see which queue and topics are created run:

```
docker-compose -f local/docker-compose-localDev.yml logs provision
```

Output:
```
provision_1   | make_bucket: sample-bucket
provision_1   | Created S3 bucket s3://sample-bucket
```
## Test
Tests can be performed from your host with the [aws cli](https://aws.amazon.com/documentation/cli/).
Note that S3 is running on port 4572 on localhost.
To list files stored into bucket with name 'sample-bucket' and having prefix 'in/' run:

```
aws --endpoint-url http://localhost:4572 s3 ls s3://sample-bucket/ --recursive
```