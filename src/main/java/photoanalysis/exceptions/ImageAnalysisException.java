package photoanalysis.exceptions;

public class ImageAnalysisException extends RuntimeException {

    public ImageAnalysisException(String s) {
        super(s);
    }

    public ImageAnalysisException(Exception e) {
        super(e);
    }
}
