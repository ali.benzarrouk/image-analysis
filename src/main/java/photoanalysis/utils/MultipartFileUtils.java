package photoanalysis.utils;

import org.springframework.web.multipart.MultipartFile;
import photoanalysis.exceptions.ImageAnalysisException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MultipartFileUtils {

    public static File multipartToFile(MultipartFile multipart)
    {
        try {
            if (multipart == null) {
                return null;
            }
            File convertedFile = new File(multipart.getOriginalFilename());

            convertedFile.createNewFile();

            FileOutputStream fos = new FileOutputStream(convertedFile);
            fos.write(multipart.getBytes());
            fos.close();

            return convertedFile;
        } catch (IOException e) {
            throw new ImageAnalysisException(e);
        }
    }
}
