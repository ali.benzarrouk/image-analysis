package photoanalysis.model;

/**
 * An application info POJO.
 */
public class CustomAppInfo {
    private String version;
    private String uptime;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }
}
