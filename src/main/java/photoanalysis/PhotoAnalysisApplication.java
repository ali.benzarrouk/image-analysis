package photoanalysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.integration.annotation.IntegrationComponentScan;

@SpringBootApplication
@IntegrationComponentScan
public class PhotoAnalysisApplication extends SpringBootServletInitializer {

    /**
     * The main method for this application. Will run the Spring Application.
     * @param args - Args.
     */
    public static void main(String[] args) {
        SpringApplication.run(PhotoAnalysisApplication.class, args);
    }

    /**
     * Configures the application.
     * @param application -  Application.
     * @return  -  {@link SpringApplicationBuilder}.
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PhotoAnalysisApplication.class);
    }
}