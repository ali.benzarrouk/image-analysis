package photoanalysis.connectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import photoanalysis.configuration.aws.s3.S3Gateway;

import java.io.File;

@Profile({ "embedded", "prod", "docker"})
@Service
public class S3Connector {

    @Value("${processing-s3-bucket}")
    private String bucketName;

    private S3Gateway s3Gateway;

    @Autowired
    public S3Connector(S3Gateway s3Gateway) {
        this.s3Gateway = s3Gateway;
    }

    public void uploadFile(File file, String folder) {
        s3Gateway.upload(file, folder, file.getName(), bucketName);
    }

}
