package photoanalysis.actuator;

import photoanalysis.PhotoAnalysisApplication;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;

/**
 * Build an application uptime and version info.
 */
@Component
public class AppInfoBuilder {

    /**
     * Get an application uptime.
     *
     * @return a string representation of application uptime in milliseconds
     */
    public String getUptime() {
        return String.valueOf(ManagementFactory.getRuntimeMXBean().getUptime());
    }

    /**
     * Get an application version.
     *
     * @return an application version
     */
    public String getApplicationVersion() {
        return PhotoAnalysisApplication.class.getPackage().getImplementationVersion();
    }
}
