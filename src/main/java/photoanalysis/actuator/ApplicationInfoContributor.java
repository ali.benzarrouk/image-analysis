package photoanalysis.actuator;

import photoanalysis.model.CustomAppInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

/**
 * Generates application information.
 */
@Component
public class ApplicationInfoContributor implements InfoContributor {

    private final AppInfoBuilder appInfoBuilder;
    private CustomAppInfo customAppInfo;

    @Autowired
    public ApplicationInfoContributor(AppInfoBuilder infoBuilder) {
        this.appInfoBuilder = infoBuilder;
    }

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("custom", collectCustomInfo());
    }

    private CustomAppInfo collectCustomInfo() {
        customAppInfo = new CustomAppInfo();
        customAppInfo.setVersion(appInfoBuilder.getApplicationVersion());
        customAppInfo.setUptime(appInfoBuilder.getUptime());
        return customAppInfo;
    }

    public CustomAppInfo getCustomInfo() {
        return customAppInfo;
    }
}
