package photoanalysis.controllers;

import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;
import photoanalysis.exceptions.ImageAnalysisException;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * Exception handler extension for controllers in {@link photoanalysis.controllers}.
 */
@ControllerAdvice(basePackageClasses = ImageAnalysisController.class)
public class RestControllerExceptionHandler extends ResponseEntityExceptionHandler {
    private void handleInternalServerError(Exception e, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, e, WebRequest.SCOPE_REQUEST);
        }
    }

    private ResponseEntity<Object> handleMissingRequiredRequestParameter(ServletException e,
                                                                         String parameterName) {
        logger.error("Missing request parameter", e);
        return ResponseEntity.badRequest().body(String.format("Required parameter %s is missing.", parameterName));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException e,
                                                                          HttpHeaders headers,
                                                                          HttpStatus status,
                                                                          WebRequest request) {

        handleInternalServerError(e, status, request);
        return handleMissingRequiredRequestParameter(e, e.getParameterName());
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException e,
                                                        HttpHeaders headers,
                                                        HttpStatus status,
                                                        WebRequest request) {
        handleInternalServerError(e, status, request);
        String name = ((MethodArgumentTypeMismatchException) e).getName();
        logger.error("Type mismatch", e);
        return ResponseEntity.badRequest()
                .body(String.format("Parameter %s has an invalid %s value", name, e.getValue()));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException e,
                                                                     HttpHeaders headers,
                                                                     HttpStatus status,
                                                                     WebRequest request) {
        handleInternalServerError(e, status, request);
        return handleMissingRequiredRequestParameter(e, e.getRequestPartName());
    }

    /**
     * Method handling exceptions of type {@link ConstraintViolationException}.
     *
     * @param ex - The exception.
     * @param request - Web request that generated the exception.
     * @return a response that will be returned by the api call.
     */
    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        return ResponseEntity.badRequest().body(buildContstraintViolationMessage(ex));
    }

    /**
     * Method handling exceptions of type {@link ImageAnalysisException}.
     *
     * @param ex - The exception.
     * @param request - Web request that generated the exception.
     * @return a response that will be returned by the api call.
     */
    @ExceptionHandler({ImageAnalysisException.class})
    public ResponseEntity<Object> handleImageAnalysisException(ImageAnalysisException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    /**
     * Method handling exceptions of type {@link DataAccessException} and {@link IOException}.
     *
     * @param ex - The exception.
     * @param request - Web request that generated the exception.
     * @return {@link HttpStatus#INTERNAL_SERVER_ERROR} response
     */
    @ExceptionHandler({DataAccessException.class, IOException.class})
    public ResponseEntity<Object> handleIOandDataAccess(Exception ex, WebRequest request) {
        return buildResponseEntity(ex, request, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> buildResponseEntity(Exception ex, WebRequest request, HttpStatus status) {
        logger.error(ex.getMessage(), ex);
        request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        return new ResponseEntity<>(ex.getMessage(), status);
    }

    private String buildContstraintViolationMessage(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> set = ex.getConstraintViolations();
        return set.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(","));
    }
}

