package photoanalysis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import photoanalysis.configuration.validators.constraints.ArchiveWithImagesConstraint;
import photoanalysis.services.ImageAnalysisService;

import javax.validation.Valid;

@RestController
@Validated
public class ImageAnalysisController {

    private ImageAnalysisService imageAnalysisService;

    @Autowired
    public ImageAnalysisController(ImageAnalysisService imageAnalysisService) {
        this.imageAnalysisService = imageAnalysisService;
    }

    /**
     * Start processing archive.
     *
     * @param file - Zip file.
     * @return ID of request.
     */
    @RequestMapping(path = "/analyze", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> analyseImages(@Valid @ArchiveWithImagesConstraint @RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(imageAnalysisService.startProcessing(file));
    }
}
