package photoanalysis.configuration.enums;

public enum MessageHeader {
    REQUEST_ID("Request_ID");

    private String value;

    MessageHeader(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
