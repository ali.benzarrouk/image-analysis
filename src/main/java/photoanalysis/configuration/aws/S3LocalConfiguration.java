package photoanalysis.configuration.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.context.annotation.ConditionalOnMissingAmazonClient;
import org.springframework.cloud.aws.core.config.AmazonWebserviceClientFactoryBean;
import org.springframework.cloud.aws.core.region.RegionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Configuration for the local S3 client.
 */
@Configuration
@Profile({"embedded"})
public class S3LocalConfiguration {
    @Value("${aws.s3.port}")
    private int s3LocalPort;

    @Value("${aws.s3.endpoint}")
    private String s3LocalEndpoint;

    /**
     * Create an instance of the AmazonS3 factory.
     * Invoked only if the client with the same name does not exist already.
     *
     * @param awsCredentialsProvider - AWS credential provider
     * @return an instance of the local factory that has a {@link AmazonS3Client} parameter
     */
    @ConditionalOnMissingAmazonClient(AmazonS3.class)
    @Bean
    public AmazonWebserviceClientFactoryBean<AmazonS3Client> amazonS3(
            AWSCredentialsProvider awsCredentialsProvider,
            RegionProvider regionProvider) {
        return new AmazonWebserviceClientLocalFactoryBean<>(AmazonS3Client.class,
                awsCredentialsProvider, false, s3LocalEndpoint, regionProvider.getRegion().getName(), s3LocalPort);
    }
}
