package photoanalysis.configuration.aws;

import com.amazonaws.auth.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.core.region.StaticRegionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * AWS configuration.
 */
@Profile({"prod", "embedded"})
@Configuration
public class AwsConfiguration {
    @Value("${cloud.aws.region.static}")
    private String region;

    @Value("${aws.accessKeyId}")
    private String accessKey;

    @Value("${aws.secretKey}")
    private String secretKey;

    /**
     * Create an {@link AWSCredentialsProviderChain} that contains three
     * credential providers - {@link InstanceProfileCredentialsProvider},
     * {@link PropertiesFileCredentialsProvider} and {@link AWSStaticCredentialsProvider} respectively.
     * First look into the amazon instance for an instance profile.
     * If that fails, look for the credentials file. The system assumes that the path
     * relative to the users home directory is provided. Failing that lookup the system provides the clients
     * with a pair od dummy username and password intended to be used with a local instance.
     *
     * @return an {@link AWSCredentialsProviderChain} instance
     */
    @Bean
    public AWSCredentialsProvider awsCredentialsProvider() {
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey));
    }

    @Bean
    public StaticRegionProvider staticRegionProvider() {
        return new StaticRegionProvider(region);
    }
}
