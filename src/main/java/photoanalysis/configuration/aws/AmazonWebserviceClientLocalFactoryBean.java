package photoanalysis.configuration.aws;

import com.amazonaws.AmazonWebServiceClient;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import org.springframework.cloud.aws.core.config.AmazonWebserviceClientFactoryBean;
import org.springframework.context.annotation.Profile;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

/**
 * A client factory specialization. This is used for local profiles.
 *
 * @param <T> a subclass of the {@link AmazonWebServiceClient}
 */
@Profile({"prod", "embedded"})
public class AmazonWebserviceClientLocalFactoryBean<T extends AmazonWebServiceClient>
        extends AmazonWebserviceClientFactoryBean<T> {
    private final AWSCredentialsProvider credentialsProvider;
    private final boolean ssl;
    private final String endpoint;
    private final String region;
    private final int port;

    /**
     * Create an instance of this factory by providing an ssl flag, credential provider, endpoint and port.
     *
     * @param credentialsProvider - credential provider for the local environment
     * @param ssl - should the connection be SSL protected
     * @param endpoint request endpoint
     * @param region request region
     * @param port request port
     */
    public AmazonWebserviceClientLocalFactoryBean(Class<T> clientClass,
                                                  AWSCredentialsProvider credentialsProvider,
                                                  boolean ssl,
                                                  String endpoint,
                                                  String region,
                                                  Integer port) {
        super(clientClass, credentialsProvider);
        this.ssl = ssl;
        this.endpoint = endpoint;
        this.port = port;
        this.region = region;
        this.credentialsProvider = credentialsProvider;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected T createInstance() {
        String uri;
        if (ssl) {
            uri = "https://";
        } else {
            uri = "http://";
        }

        String builderName = this.getObjectType().getName() + "Builder";
        Class<?> className = ClassUtils.resolveClassName(builderName, ClassUtils.getDefaultClassLoader());

        Method method = ClassUtils.getStaticMethod(className, "standard");
        Assert.notNull(method, String.format(
                "Could not find %s method in class '%s'",
                "standard()",
                className.getName()));

        // Need to use reflection here (see parent class)
        // in order to invoke the static method from
        // a dynamically generated builder class name.
        AwsClientBuilder<?, T> builder = (AwsClientBuilder<?, T>) ReflectionUtils.invokeMethod(method, null);
        builder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
                uri + endpoint + ":" + port, region
        ));

        builder.withCredentials(this.credentialsProvider);

        // This is necessary to make S3 work locally.
        // By default the bucket url is build like s3://<bucket-name>.foo-bar.com
        // This is something that can't work locally without editing the hosts file.
        // By setting this attribute we are instructing the S3 client to use s3://foo-bar.com/<bucket-name> syntax
        Method pathStyleEnabledMethod =
                ClassUtils.getMethodIfAvailable(className, "withPathStyleAccessEnabled", Boolean.class);
        if (pathStyleEnabledMethod != null) {
            ReflectionUtils.invokeMethod(pathStyleEnabledMethod, builder, true);
        }

        return builder.build();
    }
}
