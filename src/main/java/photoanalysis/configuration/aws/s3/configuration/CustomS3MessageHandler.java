package photoanalysis.configuration.aws.s3.configuration;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.integration.aws.outbound.S3MessageHandler;
import org.springframework.integration.expression.ExpressionUtils;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Augment {@link S3MessageHandler} functionality.
 * Adds additional 'list' and 'delete' capabilities to the base class.
 */
@Profile({"prod", "embedded"})
public class CustomS3MessageHandler extends S3MessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomS3MessageHandler.class);
    private static final String DELETE_FILE_ERROR_MSG = "Deleting file failed: {}{}, Error Message: {}, HTTP Status Code: {}";
    private static final String KEY_EXPRESSION_ERROR_MSG = "'extendedKeyExpression' must not be null";
    private static final String BUCKET_EXPRESSION_ERROR_MSG = "'bucketExpression' must not be null";

    private Expression extendedKeyExpression;
    private Expression bucketExpression;
    private AmazonS3 amazonS3;
    private EvaluationContext evaluationContext;

    private Expression extendedCommandExpression = new ValueExpression<>(ExtendedCommand.NOT_SUPPORTED);

    /**
     * A constructor.
     */
    CustomS3MessageHandler(AmazonS3 amazonS3, Expression bucketExpression, boolean produceReply) {
        super(amazonS3, bucketExpression, produceReply);
        this.amazonS3 = amazonS3;
        this.bucketExpression = bucketExpression;
    }

    @Override
    protected void doInit() {
        super.doInit();
        this.evaluationContext = ExpressionUtils.createStandardEvaluationContext(getBeanFactory());
    }

    @Override
    protected Object handleRequestMessage(Message<?> requestMessage) {
        ExtendedCommand command = this.extendedCommandExpression.getValue(this.evaluationContext, requestMessage, ExtendedCommand.class);

        switch (Objects.requireNonNull(command)) {
        case LIST:
            return listFiles(requestMessage);

        case DELETE:
            return deleteFile(requestMessage);

        default:
            return super.handleRequestMessage(requestMessage);
        }
    }

    private String getKey(Message<?> requestMessage) {
        Assert.notNull(extendedKeyExpression, KEY_EXPRESSION_ERROR_MSG);
        return extendedKeyExpression.getValue(this.evaluationContext, requestMessage, String.class);
    }

    private String getBucket(Message<?> requestMessage) {
        Assert.notNull(bucketExpression, BUCKET_EXPRESSION_ERROR_MSG);

        return bucketExpression.getValue(this.evaluationContext, requestMessage, String.class);
    }

    /**
     * Returns a list of file names under specified bucket and folder. According to underlying amazon implementation list
     * results are <i>always</i> returned in lexicographic (alphabetical) order.
     *
     * @param requestMessage - the message
     * @return {@link List} of file names
     *
     */
    List<String> listFiles(Message<?> requestMessage) {
        String key = getKey(requestMessage);
        String bucketName = getBucket(requestMessage);

        try {
            List<String> list = new ArrayList<>();
            ListObjectsV2Request listObjectsRequest = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(key);
            ListObjectsV2Result result;

            do {
                result = amazonS3.listObjectsV2(listObjectsRequest);
                for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
                    list.add(objectSummary.getKey());
                }
                listObjectsRequest.setContinuationToken(result.getNextContinuationToken());
            } while (result.isTruncated());

            return list;
        } catch (AmazonServiceException e) {
            LOGGER.error("Listing of objects in bucket {} failed", bucketName, e);
            throw e;
        }
    }

    /**
     * Deletes an S3 file. The operation is idempotent i.e. if attempting to delete an object that does not exist,
     * Amazon S3 returns a success message instead of an error message.
     *
     * @param requestMessage - the message
     * @return <code>null</code> on successful file deletion
     */
    Object deleteFile(Message<?> requestMessage) {
        String key = getKey(requestMessage);
        String bucketName = getBucket(requestMessage);

        try {
            amazonS3.deleteObject(bucketName, key);
        } catch (AmazonServiceException ase) {
            LOGGER.error(DELETE_FILE_ERROR_MSG, bucketName, key, ase.getMessage(), ase.getStatusCode(), ase);
            throw ase;
        } catch (AmazonClientException ace) {
            LOGGER.error(DELETE_FILE_ERROR_MSG, bucketName, key, ace);
            throw ace;
        }
        return null;
    }

    /**
     * Extended s3 commands.Extension to the available
     * Command.UPLOAD, Command.DOWNLOAD and Command.COPY commands.
     */
    public enum ExtendedCommand {

        /**
         * S3 bucket list command.
         */
        LIST,

        /**
         * Delete file command.
         */
        DELETE,

        /**
         * Extended command not supported.
         */
        NOT_SUPPORTED
    }

    void setExtendedKeyExpression(Expression extendedKeyExpression) {
        this.extendedKeyExpression = extendedKeyExpression;
    }

    void setExtendedCommandExpression(Expression extendedCommandExpression) {
        this.extendedCommandExpression = extendedCommandExpression;
    }
}
