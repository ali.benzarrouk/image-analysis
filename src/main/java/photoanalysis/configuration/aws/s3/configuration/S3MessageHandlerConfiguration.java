package photoanalysis.configuration.aws.s3.configuration;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;

/**
 * S3 configuration.
 */
@Profile({"prod", "embedded"})
@Configuration
public class S3MessageHandlerConfiguration {

    /**
     * Channel used to list bucket content.
     */
    public static final String LIST_FILE_CHANNEL = "listFolderChannel";

    /**
     * Channel used for S3 file deletion.
     */
    public static final String DELETE_FILE_CHANNEL = "deleteFileChannel";

    /**
     * Channel used for S3 file download.
     */
    public static final String DOWNLOAD_FILE_CHANNEL = "downloadFileChannel";

    /**
     * Channel used for S3 file upload.
     */
    public static final String UPLOAD_FILE_CHANNEL = "uploadFileChannel";

    private AmazonS3 amazonS3;
    private final S3MessageHandlers s3MessageHandlers = new S3MessageHandlers();

    /**
     * Constructor.
     *
     * @param amazonS3 - S3 client interface.
     */
    @Autowired
    public S3MessageHandlerConfiguration(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    /**
     * S3 list handler.
     *
     * @return instance of {@link MessageHandler}
     */
    @Bean
    @ServiceActivator(inputChannel = LIST_FILE_CHANNEL)
    public MessageHandler s3ListMessageHandler() {
        return s3MessageHandlers.listMessageHandler(amazonS3);
    }

    /**
     * S3 delete handler.
     *
     * @return instance of {@link MessageHandler}
     */
    @Bean
    @ServiceActivator(inputChannel = DELETE_FILE_CHANNEL)
    public MessageHandler s3DeleteMessageHandler() {
        return s3MessageHandlers.deleteMessageHandler(amazonS3);
    }

    /**
     * S3 download handler.
     *
     * @return instance of {@link MessageHandler}
     */
    @Bean
    @ServiceActivator(inputChannel = DOWNLOAD_FILE_CHANNEL)
    public MessageHandler s3DownloadMessageHandler() {
        return s3MessageHandlers.downloadMessageHandler(amazonS3);
    }

    /**
     * S3 upload handler.
     *
     * @return instance of {@link MessageHandler}
     */
    @Bean
    @ServiceActivator(inputChannel = UPLOAD_FILE_CHANNEL)
    public MessageHandler s3UploadMessageHandler() {
        return s3MessageHandlers.uploadMessageHandler(amazonS3);
    }
}

