package photoanalysis.configuration.aws.s3.configuration;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.context.annotation.Profile;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.aws.outbound.S3MessageHandler;
import org.springframework.integration.aws.outbound.S3MessageHandler.Command;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.messaging.MessageHandler;

/**
 * A factory for s3 message handlers.
 */
@Profile({"prod", "embedded"})
public class S3MessageHandlers {

    private static final SpelExpressionParser PARSER = new SpelExpressionParser();
    private static final String HEADER_BUCKET_EXPRESSION = "headers['bucket_name']";

    /**
     * S3 list handler.
     *
     * @param amazonS3 - aws service
     * @return instance of {@link MessageHandler}
     */
    public MessageHandler listMessageHandler(AmazonS3 amazonS3) {
        Expression keyExpression = PARSER.parseExpression("headers['s3_folder']");
        Expression bucketExpression = PARSER.parseExpression(HEADER_BUCKET_EXPRESSION);
        CustomS3MessageHandler handler = new CustomS3MessageHandler(amazonS3, bucketExpression, false);
        handler.setExtendedKeyExpression(keyExpression);
        handler.setExtendedCommandExpression(new ValueExpression<>(CustomS3MessageHandler.ExtendedCommand.LIST));
        return handler;
    }

    /**
     * S3 delete handler.
     *
     * @param amazonS3 - aws service
     * @return instance of {@link MessageHandler}
     */
    public MessageHandler deleteMessageHandler(AmazonS3 amazonS3) {
        Expression keyExpression = PARSER.parseExpression("headers['filename']");
        Expression bucketExpression = PARSER.parseExpression(HEADER_BUCKET_EXPRESSION);
        CustomS3MessageHandler handler = new CustomS3MessageHandler(amazonS3, bucketExpression, false);
        handler.setExtendedKeyExpression(keyExpression);
        handler.setExtendedCommandExpression(new ValueExpression<>(CustomS3MessageHandler.ExtendedCommand.DELETE));
        return handler;
    }

    /**
     * S3 download handler.
     *
     * @param amazonS3 - aws service
     * @return instance of {@link MessageHandler}
     */
    public MessageHandler downloadMessageHandler(AmazonS3 amazonS3) {
        Expression bucketExpression = PARSER.parseExpression(HEADER_BUCKET_EXPRESSION);
        Expression keyExpression = PARSER.parseExpression("headers['s3_folder'] + payload.getName()");
        S3MessageHandler handler = new S3MessageHandler(amazonS3, bucketExpression, false);
        handler.setKeyExpression(keyExpression);
        handler.setCommandExpression(new ValueExpression<>(Command.DOWNLOAD));
        return handler;
    }

    /**
     * S3 upload handler.
     *
     * @param amazonS3 - aws service
     * @return instance of {@link MessageHandler}
     */
    public MessageHandler uploadMessageHandler(AmazonS3 amazonS3) {
        Expression keyExpression = PARSER.parseExpression("headers['s3_folder'] + headers['filename']");
        Expression bucketExpression = PARSER.parseExpression(HEADER_BUCKET_EXPRESSION);
        S3MessageHandler handler = new S3MessageHandler(amazonS3, bucketExpression, false);
        handler.setKeyExpression(keyExpression);
        handler.setCommandExpression(new ValueExpression<>(Command.UPLOAD));
        return handler;
    }
}
