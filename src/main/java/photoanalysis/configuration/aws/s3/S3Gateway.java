package photoanalysis.configuration.aws.s3;

import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.io.File;
import java.util.Collection;

import static photoanalysis.configuration.aws.s3.configuration.S3MessageHandlerConfiguration.*;

/**
 * S3 operations gateway.
 */
@Profile({"prod", "embedded"})
@MessagingGateway
public interface S3Gateway {

    /**
     * List files in specified S3 folder.
     *
     * @param s3Folder - the s3 folder to be used
     * @param bucket - the bucket to upload into
     * @return {@link Collection} of file names
     */
    @Gateway(requestChannel = LIST_FILE_CHANNEL)
    @Payload("''")
    Collection<String> list(@Header("s3_folder") String s3Folder, @Header("bucket_name") String bucket);

    /**
     * Download S3 file.
     *
     * @param file - the file on the local system to which s3 file will be download.
     * @param s3Folder - S3 folder from which to download file
     * @param fileName - the name of the file to download
     * @param bucket - the S3 bucket to download from
     */
    @Gateway(requestChannel = DOWNLOAD_FILE_CHANNEL)
    void download(File file, @Header("s3_folder") String s3Folder, @Header("filename") String fileName, @Header("bucket_name") String bucket);

    /**
     * Deletes specified file from given S3 bucket.
     *
     * @param fileName - file to be deleted
     * @param bucket - the S3 bucket
     */
    @Gateway(requestChannel = DELETE_FILE_CHANNEL)
    @Payload("''")
    void delete(@Header("filename") String fileName, @Header("bucket_name") String bucket);

    /**
     * Uploads file to archive.
     *
     * @param content - the file data to be uploaded
     * @param s3Folder - the s3 folder to be used
     * @param bucket - target bucket
     */
    @Gateway(requestChannel = UPLOAD_FILE_CHANNEL)
    void upload(File content, @Header("s3_folder") String s3Folder, @Header("filename") String fileName, @Header("bucket_name") String bucket);
}

