package photoanalysis.configuration.channels;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class ArchiveProcessingChannelConfiguration {
    public final static String ARCHIVE_PROCESSING_CHANNEL = "ArchiveProcessingChannel";
    public final static String BACKUP_CHANNEL = "BackUpChannel";
    public final static String PREPARING_CHANNEL = "ZipPreparingChannel";

    @Bean(name = ARCHIVE_PROCESSING_CHANNEL)
    public MessageChannel archiveProcessingChannel() {
        return new DirectChannel();
    }

    @Bean(name = BACKUP_CHANNEL)
    public MessageChannel backUpChannel() {
        return new DirectChannel();
    }

    @Bean(name = PREPARING_CHANNEL)
    public MessageChannel zipPreparingChannel() {
        return new DirectChannel();
    }
}
