package photoanalysis.configuration.validators;

import org.aspectj.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import photoanalysis.configuration.validators.constraints.ArchiveWithImagesConstraint;

import javax.imageio.ImageIO;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;

import static photoanalysis.utils.MultipartFileUtils.multipartToFile;

public class ImageArchiveValidator implements ConstraintValidator<ArchiveWithImagesConstraint, MultipartFile> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageArchiveValidator.class);

    @Override
    public void initialize(ArchiveWithImagesConstraint archiveWithImagesConstraint) {

    }

    @Override
    public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext constraintValidatorContext) {
        File file;
        try {
            file = multipartToFile(multipartFile);
        } catch (RuntimeException e) {
            LOGGER.error("Error while converting multipart file {} to file.",multipartFile.getName(), e);
            return false;
        }

        if (file == null) {
            return false;
        }

        file.deleteOnExit();

        return isFileReadableArchive(file) && isArchiveContentImages(file);
    }

    private boolean isFileReadableArchive(File file) {
        return FileUtil.canReadFile(file) && FileUtil.isZipFile(file);
    }

    private boolean isArchiveContentImages(File archive) {
        ZipFile archiveZip;
        try {
            archiveZip = new ZipFile(archive);
        } catch (IOException e) {
            LOGGER.error("{} not zip file.",archive.getName(), e);
            return false;
        }

        return archiveZip.stream().parallel().allMatch(zipEntry -> {
            try {
                InputStream is = archiveZip.getInputStream(zipEntry);
                return ImageIO.read(is) != null;
            } catch (IOException e) {
                LOGGER.error("Error while verifying zip entry {} is an image.", zipEntry.getName(), e);
                return false;
            }
        });
    }
}
