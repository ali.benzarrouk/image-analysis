package photoanalysis.configuration.validators.constraints;

import photoanalysis.configuration.validators.ImageArchiveValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ImageArchiveValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ArchiveWithImagesConstraint {
    String message() default "Invalid archive";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
