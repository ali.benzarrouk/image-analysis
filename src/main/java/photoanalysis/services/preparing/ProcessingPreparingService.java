package photoanalysis.services.preparing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import photoanalysis.configuration.enums.MessageHeader;

import java.io.File;

import static photoanalysis.configuration.channels.ArchiveProcessingChannelConfiguration.PREPARING_CHANNEL;

@Service
public class ProcessingPreparingService {

    private IPreparingComponent preparingService;

    @Autowired
    public ProcessingPreparingService(IPreparingComponent preparingService) {
        this.preparingService = preparingService;
    }

    @ServiceActivator(inputChannel = PREPARING_CHANNEL)
    public void prepareProcessing(Message<File> fileMessage) {
        preparingService.prepareProcessingDataStructure(fileMessage.getHeaders().get(MessageHeader.REQUEST_ID.getValue()).toString(),
                fileMessage.getPayload());
    }
}
