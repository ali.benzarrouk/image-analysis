package photoanalysis.services.preparing;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import photoanalysis.exceptions.ImageAnalysisException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.zip.ZipFile;

@Component
@Profile("local")
public class LocalFolderPreparingComponent implements IPreparingComponent {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalFolderPreparingComponent.class);
    private static final String TMP = "tmp";

    @Value("${processing-data-structure-local-folder}")
    private String processingLocalFolder;

    @Value("${waiting-for-processing-folder}")
    private String waitingForProcessingFolder;

    @Value("${in-processing-folder}")
    private String inProcessingFolder;

    @Value("${processed-folder}")
    private String processedFolder;

    @Override
    public void prepareProcessingDataStructure(String requestId, File zipFile) {
        try {
            ZipFile archiveZip = new ZipFile(zipFile);

            archiveZip
                    .stream()
                    .parallel()
                    .forEach(zipEntry -> {
                        try {
                            FileUtils.copyInputStreamToFile(archiveZip.getInputStream(zipEntry),
                                    Paths.get(processingLocalFolder,
                                            requestId,
                                            TMP,
                                            waitingForProcessingFolder,
                                            zipEntry.getName()).toFile());
                        } catch (IOException e) {
                            LOGGER.error("Error while unzipping entry {}.", zipEntry.getName(), e);
                            throw new ImageAnalysisException(e);
                        }
                    });

            Paths.get(processingLocalFolder,
                    requestId + TMP,
                    inProcessingFolder)
                    .toFile()
                    .mkdirs();

            Paths.get(processingLocalFolder,
                    requestId + TMP,
                    processedFolder)
                    .toFile()
                    .mkdirs();

            Paths.get(processingLocalFolder,
                    requestId + TMP)
                    .toFile()
                    .renameTo(Paths.get(processingLocalFolder,
                            requestId)
                            .toFile());
        } catch (IOException e) {
            LOGGER.error("IO exception while preparing folder structure for request ID {} of file {}.",
                    requestId,
                    zipFile.getName(),
                    e);
            throw new ImageAnalysisException(e);
        }
    }
}
