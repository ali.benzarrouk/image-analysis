package photoanalysis.services.preparing;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import photoanalysis.connectors.S3Connector;
import photoanalysis.exceptions.ImageAnalysisException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.zip.ZipFile;

@Component
@Profile({ "embedded", "prod" })
public class S3PreparingComponent implements IPreparingComponent {
    private static final Logger LOGGER = LoggerFactory.getLogger(S3PreparingComponent.class);

    private S3Connector s3Connector;

    @Autowired
    public S3PreparingComponent(S3Connector s3Connector) {
        this.s3Connector = s3Connector;
    }

    @Override
    public void prepareProcessingDataStructure(String requestId, File zipFile) {
        try {
            ZipFile archiveZip = new ZipFile(zipFile);

            archiveZip
                    .stream()
                    .parallel()
                    .forEach(zipEntry -> {
                        try {
                            File tmpFile = Paths.get(zipEntry.getName()).toFile();
                            FileUtils.copyInputStreamToFile(archiveZip.getInputStream(zipEntry),
                                    tmpFile);
                            s3Connector.uploadFile(tmpFile, requestId + "/");
                        } catch (IOException e) {
                            LOGGER.error("Error while unzipping entry {}.", zipEntry.getName(), e);
                            throw new ImageAnalysisException(e);
                        }
                    });

        } catch (IOException e) {
            LOGGER.error("IO exception while preparing folder structure for request ID {} of file {}.",
                    requestId,
                    zipFile.getName(),
                    e);
            throw new ImageAnalysisException(e);
        }

    }
}
