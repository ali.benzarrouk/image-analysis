package photoanalysis.services.preparing;


import java.io.File;

public interface IPreparingComponent {
    void prepareProcessingDataStructure(String requestId, File zipFile);
}
