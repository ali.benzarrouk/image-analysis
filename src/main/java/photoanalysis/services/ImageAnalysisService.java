package photoanalysis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import photoanalysis.configuration.enums.MessageHeader;
import photoanalysis.exceptions.ImageAnalysisException;

import java.io.File;
import java.util.UUID;

import static photoanalysis.configuration.channels.ArchiveProcessingChannelConfiguration.ARCHIVE_PROCESSING_CHANNEL;
import static photoanalysis.utils.MultipartFileUtils.multipartToFile;

@Service
public class ImageAnalysisService {

    private MessageChannel imagesArchiveProcessingChannel;

    @Autowired
    public ImageAnalysisService(@Qualifier(ARCHIVE_PROCESSING_CHANNEL) MessageChannel imagesArchiveProcessingChannel) {
        this.imagesArchiveProcessingChannel = imagesArchiveProcessingChannel;
    }

    /**
     * Start processing zip file.
     *
     * @param inputMultiPartFile - Input archive containing photos to process.
     * @return  - ID of the request.
     */
    public String startProcessing(MultipartFile inputMultiPartFile) {

        File inputFile = multipartToFile(inputMultiPartFile);
        if (inputFile == null)
            throw new ImageAnalysisException("Problem with transforming multipart file to file");

        UUID uuid = UUID.randomUUID();

        Message<File> fileMessage = MessageBuilder
                .withPayload(inputFile)
                .setHeader(MessageHeader.REQUEST_ID.getValue(), uuid)
                .build();

        imagesArchiveProcessingChannel.send(fileMessage);

        return uuid.toString();
    }
}
