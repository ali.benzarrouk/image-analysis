package photoanalysis.services.backup;

import org.apache.commons.io.FileUtils;
import org.aspectj.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import photoanalysis.exceptions.ImageAnalysisException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@Profile("local")
@Component
public class LocalFolderBackUpComponent implements IBackUpComponent {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalFolderBackUpComponent.class);
    private static final String INTERNAL_ERROR = "Internal Error";

    @Value("${backup-folder}")
    private String backUpFolderPath;

    @PostConstruct
    private void postConstruct() {
        File backUpFolder = new File(backUpFolderPath);
        if (!FileUtil.canWriteDir(backUpFolder)) {
            LOGGER.error("Back up folder {} non accessible.", backUpFolderPath);
            throw new ImageAnalysisException(INTERNAL_ERROR);
        }
    }

    @Override
    public void backUp(String requestId, File file) {
        try {
            FileUtils.copyFile(file, Paths.get(backUpFolderPath, requestId, file.getName()).toFile());
        } catch (IOException e) {
            LOGGER.error("Error while backing up {}", file.getName(), e);
            throw new ImageAnalysisException(INTERNAL_ERROR);
        }
    }
}
