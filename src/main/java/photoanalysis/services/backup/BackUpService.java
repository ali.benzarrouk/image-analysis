package photoanalysis.services.backup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import photoanalysis.configuration.enums.MessageHeader;

import java.io.File;
import java.util.Objects;

import static photoanalysis.configuration.channels.ArchiveProcessingChannelConfiguration.BACKUP_CHANNEL;

@Service
public class BackUpService {

    private IBackUpComponent backUpService;

    @Autowired
    public BackUpService(IBackUpComponent backUpService) {
        this.backUpService = backUpService;
    }

    @ServiceActivator(inputChannel = BACKUP_CHANNEL)
    public void backUp(Message<File> fileMessage) {
        backUpService.backUp(Objects.requireNonNull(fileMessage.getHeaders().get(MessageHeader.REQUEST_ID.getValue())).toString(),
                fileMessage.getPayload());
    }
}
