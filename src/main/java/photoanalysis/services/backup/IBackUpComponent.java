package photoanalysis.services.backup;

import java.io.File;

public interface IBackUpComponent {
    void backUp(String requestId, File fileMessage);
}
