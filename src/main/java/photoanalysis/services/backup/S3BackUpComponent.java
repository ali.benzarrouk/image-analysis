package photoanalysis.services.backup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import photoanalysis.connectors.S3Connector;

import java.io.File;
import java.nio.file.Paths;

@Profile({"embedded", "prod"})
@Component
public class S3BackUpComponent implements IBackUpComponent {

    private S3Connector s3Connector;

    @Value("${backup-folder}")
    private String backUpFolderPath;

    @Autowired
    public S3BackUpComponent(S3Connector s3Connector) {
        this.s3Connector = s3Connector;
    }

    @Override
    public void backUp(String requestId, File fileMessage) {
        s3Connector.uploadFile(fileMessage, Paths.get(backUpFolderPath, requestId, fileMessage.getName()).toString());
    }
}
