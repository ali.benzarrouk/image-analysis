package photoanalysis.services;

import org.springframework.integration.annotation.Router;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.io.File;

import static photoanalysis.configuration.channels.ArchiveProcessingChannelConfiguration.ARCHIVE_PROCESSING_CHANNEL;
import static photoanalysis.configuration.channels.ArchiveProcessingChannelConfiguration.BACKUP_CHANNEL;
import static photoanalysis.configuration.channels.ArchiveProcessingChannelConfiguration.PREPARING_CHANNEL;

@Component
public class ImageArchiveRouter {

    @Router(inputChannel = ARCHIVE_PROCESSING_CHANNEL)
    public String[] archiveAndPRocess(Message<File> fileMessage) {
        return new String[] {BACKUP_CHANNEL, PREPARING_CHANNEL};
    }
}
