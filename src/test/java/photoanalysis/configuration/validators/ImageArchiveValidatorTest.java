package photoanalysis.configuration.validators;

import com.google.common.io.Resources;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import javax.validation.ConstraintValidatorContext;
import java.io.IOException;
import java.util.Objects;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Unit test for {@link ImageArchiveValidator}.
 */
public class ImageArchiveValidatorTest {
    private ImageArchiveValidator underTest = new ImageArchiveValidator();
    private ConstraintValidatorContext constraintValidatorContext = null;

    private final static String SAMPLE_NON_ARCHIVE = "NonArchive.txt";
    private final static String ARCHIVE_WITH_NOT_ONLY_IMAGES = "archive_with_not_only_images.zip";
    private final static String ARCHIVE_ONLY_SMALL_SIZE_IMAGES = "archive_with_only_small_images.zip";
    private final static String ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE = "archive_with_many_images_different_size.zip";

    private MockMultipartFile sampleNonArchiveMultiPartFile;
    private MockMultipartFile archiveWithNotOnlyImagesMultiPartFile;
    private MockMultipartFile archiveWithOnlySmallSizeImagesMultiPartFile;
    private MockMultipartFile archiveWithDifferentSizeImagesMultiPartFile;

    public ImageArchiveValidatorTest() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();

        sampleNonArchiveMultiPartFile = new MockMultipartFile(SAMPLE_NON_ARCHIVE,
                SAMPLE_NON_ARCHIVE,
                null,
                Resources.toByteArray(Objects.requireNonNull(classLoader.getResource((SAMPLE_NON_ARCHIVE)))));

        archiveWithNotOnlyImagesMultiPartFile = new MockMultipartFile(ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE,
                ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE,
                null,
                Resources.toByteArray(Objects.requireNonNull(classLoader.getResource((ARCHIVE_WITH_NOT_ONLY_IMAGES)))));

        archiveWithOnlySmallSizeImagesMultiPartFile = new MockMultipartFile(ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE,
                ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE,
                null,
                Resources.toByteArray(Objects.requireNonNull(classLoader.getResource((ARCHIVE_ONLY_SMALL_SIZE_IMAGES)))));

        archiveWithDifferentSizeImagesMultiPartFile = new MockMultipartFile(ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE,
                ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE,
                null,
                Resources.toByteArray(Objects.requireNonNull(classLoader.getResource((ARCHIVE_WITH_IMAGES_OF_DIFFERENT_SIZE)))));
    }

    @Test
    public void isValid_onNullEntry_returnsFalse() {
        assertFalse(underTest.isValid(null, constraintValidatorContext));
    }

    @Test
    public void isValid_onNonArchive_returnsFalse() {
        assertFalse(underTest.isValid(sampleNonArchiveMultiPartFile, constraintValidatorContext));
    }

    @Test
    public void isValid_onArchiveContainingNotOnlyImages_returnsFalse() {
        assertFalse(underTest.isValid(archiveWithNotOnlyImagesMultiPartFile, constraintValidatorContext));
    }

    @Test
    public void isValid_onArchiveWithOnlySmallSizeImages_returnsTrue() {
        assertTrue(underTest.isValid(archiveWithOnlySmallSizeImagesMultiPartFile, constraintValidatorContext));
    }

    @Test
    public void isValid_onArchiveWithImagesOfDifferentSize_returnsTrue() {
        assertTrue(underTest.isValid(archiveWithDifferentSizeImagesMultiPartFile, constraintValidatorContext));
    }
}
