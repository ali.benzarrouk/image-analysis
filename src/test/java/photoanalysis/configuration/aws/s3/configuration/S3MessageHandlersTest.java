package photoanalysis.configuration.aws.s3.configuration;

import com.amazonaws.services.s3.AmazonS3;
import org.eclipse.jetty.util.ProcessorUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.MessageHandler;

import static org.eclipse.jetty.util.ProcessorUtils.availableProcessors;
import static org.junit.Assert.assertNotNull;

/**
 * Test for {@link S3MessageHandlers} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class S3MessageHandlersTest {

    private S3MessageHandlers underTest;

    @Mock
    private AmazonS3 amazonS3;

    @Before
    public void setUp() {
        underTest = new S3MessageHandlers();
    }

    @Test
    public void listMessageHandler_whenInvoked_returnInstance() {
        MessageHandler handler = underTest.listMessageHandler(amazonS3);
        assertNotNull(handler);
    }

    @Test
    public void deleteMessageHandler_whenInvoked_returnInstance() {
        MessageHandler handler = underTest.deleteMessageHandler(amazonS3);
        assertNotNull(handler);
    }

    @Test
    public void downloadMessageHandler_whenInvoked_returnInstance() {
        MessageHandler handler = underTest.downloadMessageHandler(amazonS3);
        assertNotNull(handler);
    }

    @Test
    public void uploadMessageHandler_whenInvoked_returnInstance() {
        MessageHandler handler = underTest.uploadMessageHandler(amazonS3);
        assertNotNull(handler);
    }
}
