package photoanalysis.configuration.aws.s3.configuration;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.expression.Expression;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.messaging.Message;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

/**
 * A test for {@link CustomS3MessageHandler} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomS3MessageHandlerTest {

    private CustomS3MessageHandler underTest;
    private Message<?> requestMessage;

    @Mock
    private AmazonS3 amazonS3;

    @Before
    public void setUp() {
        underTest = new CustomS3MessageHandler(amazonS3, new ValueExpression<>("bucket"), false);
        Expression keyExpression = Mockito.mock(Expression.class);
        Expression bucketExpression = Mockito.mock(Expression.class);
        ReflectionTestUtils.setField(underTest, "extendedKeyExpression", keyExpression);
        ReflectionTestUtils.setField(underTest, "bucketExpression", bucketExpression);
    }

    @Test
    public void deleteFile_whenInvoked_objectIsDeleted() {
        // act
        Object result = underTest.deleteFile(requestMessage);

        // assert
        assertNull(result);
    }

    @Test(expected = AmazonServiceException.class)
    public void listFolder_whenThrowException_theExceptionIsReThrown() {
        // arrange
        doThrow(AmazonServiceException.class).when(amazonS3).listObjectsV2(any(ListObjectsV2Request.class));

        // act
        underTest.listFiles(requestMessage);
    }

    @Test
    public void listFiles_whenInvoked_returnsListOfFiles() {
        // arrange
        ListObjectsV2Result listResult = new ListObjectsV2Result();
        S3ObjectSummary summary1 = new S3ObjectSummary();
        summary1.setKey("folder/file");
        S3ObjectSummary summary2 = new S3ObjectSummary();
        summary2.setKey("folder/secondFile");
        ReflectionTestUtils.setField(listResult, "objectSummaries", Arrays.asList(summary1, summary2));
        when(amazonS3.listObjectsV2(any(ListObjectsV2Request.class))).thenReturn(listResult);

        // act
        List<String> result = underTest.listFiles(requestMessage);

        // assert
        assertEquals("Expected result mismatch", 2, result.size());
        assertEquals("First file does not match", "folder/file", result.get(0));
        assertEquals("Second file does not match", "folder/secondFile", result.get(1));
    }
}
